set number
set undolevels=1000

" search
set incsearch
set hlsearch
set ignorecase
set smartcase

" spell check
set spell
set spellfile=~/.vim/spell/custom.utf-8.add

" use ctrl-l to clear the highlighting of :set hlsearch.
if maparg('<C-L>', 'n') ==# ''
  nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
endif
